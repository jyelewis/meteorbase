import { FlowRouter } from 'meteor/kadira:flow-router';
import { BlazeLayout } from 'meteor/kadira:blaze-layout';

//import all views
import '/imports/ui/views/layout.js';
import '/imports/ui/views/lists.js';

//register routes
FlowRouter.route('/', {
  name: 'App.home',
  action() {
    BlazeLayout.render('layout', { main: 'lists' });
  	//FlowRouter.go('/lists');
  },
});