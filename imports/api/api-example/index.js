import { Mongo } from 'meteor/mongo';
import { SimpleSchema } from 'meteor/aldeed:simple-schema';

class ListsCollection extends Mongo.Collection {
	insert(list, callback) {
		return super.insert(list, callback);
	}

	update(updateObj, callback) {
		return super.update(updateObj, callback);
	}
};

export const Lists = new ListsCollection('Lists');

Lists.schema = new SimpleSchema({
	 name: { type: String, min: 1, max: 30, optional: false }
	,created: { type: Date, optional: false }
	,modified: { type: Date, optional: false }
});
Lists.attachSchema(Lists.schema);


Lists.helpers({
	tasks() {
		return Tasks.find({
			listId: this._id
		});
	}
});









