//Dont forget to include me from the server startup file

import { Meteor } from 'meteor/meteor';
import { Lists } from './index.js';

Meteor.publish('lists.all', function(){
	return Lists.find({});
});

Meteor.publish('lists.single', function(listId){
	return Lists.find(listId);
});