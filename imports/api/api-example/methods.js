//Dont forget to include me from the server startup file

import { ValidatedMethod } from 'meteor/mdg:validated-method';
import { Lists } from './index.js';


export const newList = new ValidatedMethod({
	 name: 'lists.newList'
	,validate: new SimpleSchema({ name: Lists.schema._schema.name }).validator()
	,run(newList) {
		Lists.insert(newList);
	}
});